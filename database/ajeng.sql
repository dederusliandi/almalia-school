-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2019 at 01:14 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajeng`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(100) NOT NULL,
  `kategori_barang` varchar(100) NOT NULL,
  `tanggal_pengadaan` date NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `harga_barang` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode_barang`, `kategori_barang`, `tanggal_pengadaan`, `jumlah_barang`, `harga_barang`) VALUES
(3, 'BS', 'Perlengkapan Kantor', '2019-09-01', 5, 100000),
(4, 'CL', 'Perlengkapan Kantor', '2019-09-01', 2, 100000),
(5, 'HK', 'Buku Bacaan', '2019-09-17', 2, 10000000),
(6, 'HR', 'Perlengkapan Kantor', '2019-09-09', 3, 1000000),
(7, 'HX', 'Buku Bacaan', '2019-09-01', 3, 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_barang`
--

CREATE TABLE `detail_barang` (
  `id` int(11) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_barang`
--

INSERT INTO `detail_barang` (`id`, `kode_inventaris`, `nama_barang`, `satuan`, `status`, `id_barang`) VALUES
(1, 'BS1', 'Samsung M20', 'kg', 1, 3),
(2, 'BS2', 'HP samsung M30', 'buah', 1, 3),
(3, 'BS3', 'HP samsung M40', 'kg', 1, 3),
(4, 'BS4', 'HP samsung A20', 'kg', 1, 3),
(5, 'BS5', 'HP samsung A9', 'kg', 1, 3),
(6, 'CL1', 'Meja', 'buah', 1, 4),
(7, 'CL2', 'lemari', 'buah', 2, 4),
(8, 'HK1', 'HP samsung M30', 'kg', 1, 5),
(9, 'HK2', 'Meja', 'buah', 1, 5),
(10, 'HR1', 'laptop 1', 'buah', 1, 6),
(11, 'HR2', 'laptop 2', 'buah', 1, 6),
(12, 'HR3', 'laptop 3', 'buah', 1, 6),
(13, 'HX1', 'Meja', 'buah', 1, 7),
(14, 'HX2', 'papan tulis', 'buah', 1, 7),
(15, 'HX3', 'Meja', 'kg', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `nama`) VALUES
(1, 'admin'),
(2, 'kepala sekolah'),
(3, 'pengelola yayasan');

-- --------------------------------------------------------

--
-- Table structure for table `monitoring`
--

CREATE TABLE `monitoring` (
  `id` int(11) NOT NULL,
  `id_detail_barang` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_monitoring` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monitoring`
--

INSERT INTO `monitoring` (`id`, `id_detail_barang`, `id_user`, `tanggal_monitoring`, `status`, `keterangan`) VALUES
(1, 1, 1, '2019-09-02', 1, 'baik'),
(2, 1, 2, '2019-09-01', 1, 'mantap sekali'),
(4, 10, 2, '2019-10-01', 1, 'test'),
(5, 13, 3, '2019-10-01', 2, 'engsel rusak'),
(6, 13, 3, '2019-10-01', 1, 'sudah baik-baik saja');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama_lengkap`, `username`, `password`, `id_level`) VALUES
(1, 'Admin Almalia', 'admin', '123456', 1),
(2, 'Kepala Sekolah', 'kepalasekolah', '123456', 2),
(3, 'Pengelola Yayasan', 'pengelolayayasan', '123456', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_barang`
--
ALTER TABLE `detail_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monitoring`
--
ALTER TABLE `monitoring`
  ADD PRIMARY KEY (`id`),
  ADD KEY `monitoring_id_detail_barang_foreign` (`id_detail_barang`),
  ADD KEY `monitoring_id_user_foreign` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_level_foreign` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `detail_barang`
--
ALTER TABLE `detail_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `monitoring`
--
ALTER TABLE `monitoring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `monitoring`
--
ALTER TABLE `monitoring`
  ADD CONSTRAINT `monitoring_id_detail_barang_foreign` FOREIGN KEY (`id_detail_barang`) REFERENCES `detail_barang` (`id`),
  ADD CONSTRAINT `monitoring_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_id_level_foreign` FOREIGN KEY (`id_level`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
