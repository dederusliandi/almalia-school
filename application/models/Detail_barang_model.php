<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Detail_barang_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("detail_barang", $data);
        }

        function count($id)
        {
            return $this->db->where('id_barang', $id)->get("detail_barang")->num_rows();
        }

        function read($where = "", $order = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($where)) $this->db->order_by($order);

            $query = $this->db->get("detail_barang");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("detail_barang", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("detail_barang");
        }
    }
?>