<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventaris extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata("username")) redirect("login");
		$this->load->model('Barang_model');
		$this->load->model('Detail_barang_model');
	}

	public function index()
	{
		$data['dataInventaris'] = $this->Barang_model->read();
        $data['view'] = 'inventaris/index';
        $this->load->view('index', $data);
	}

	public function create()
	{
		$data['view'] = 'inventaris/create';
		$this->load->view('index', $data);
	}

	public function store()
	{
		$dataStore = array(
			'kode_barang' => $this->input->post('kode_barang'),
			'kategori_barang' => $this->input->post('kategori_barang'),
			'tanggal_pengadaan' => $this->input->post('tanggal_pengadaan'),
			'jumlah_barang' => $this->input->post('jumlah_barang'),
			'harga_barang' => $this->input->post('harga_barang'),
		);

		$this->Barang_model->create($dataStore);

		redirect('inventaris');
	}

	public function edit($id)
	{
		$data['view'] = 'inventaris/edit';
		$data['inventaris'] = $this->Barang_model->read("id = '$id'")[0];
		$this->load->view('index', $data);
	}

	public function update($id)
	{
		$barang = $this->Barang_model->read("id = '$id'")[0];
		$detail_barang = $this->Detail_barang_model->read("id_barang = '$id'");
		if(count($detail_barang) > $this->input->post('jumlah_barang')) {
			//feedback jika gagal
		echo "<script>
			alert('jumlah barang minimal harus sama dengan total detail barang yaitu ".count($detail_barang)."');
			window.location.href='".site_url('inventaris/edit/'.$id)."';
			</script>";
		} else {
			$dataUpdate = array(
				'kode_barang' => $this->input->post('kode_barang'),
				'kategori_barang' => $this->input->post('kategori_barang'),
				'tanggal_pengadaan' => $this->input->post('tanggal_pengadaan'),
				'jumlah_barang' => $this->input->post('jumlah_barang'),
				'harga_barang' => $this->input->post('harga_barang'),
			);

			$this->Barang_model->update("id = '$id'", $dataUpdate);
			redirect('inventaris');

		}
	}

	public function show($id)
	{
		$data['view'] = 'inventaris/show';
		$data['inventaris'] = $this->Barang_model->read("id = '$id'")[0];
		$data['dataInventaris'] = $this->Detail_barang_model->read("id_barang = '$id'");
		$this->load->view('index', $data);
	}

	public function createDetailBarang($id)
	{
		$barang = $this->Barang_model->read("id = '$id'")[0];
		$detail_barang = $this->Detail_barang_model->read("id_barang = '$id'");
		if(count($detail_barang) >= $barang->jumlah_barang) {
			//feedback jika gagal
		//feedback jika gagal
		echo "<script>
		alert('jumlah detail barang sudah memenuhi total maksimum yaitu sebanyak ".$barang->jumlah_barang."');
		window.location.href='".site_url('inventaris/show/'.$id)."';
		</script>";
		} 

		$data['kode_inventaris'] = $barang->kode_barang."".($this->Detail_barang_model->count($id) + 1);
		$data['view'] = 'detail_inventaris/create';
		$this->load->view('index', $data);

		
	}

	public function storeDetailBarang($id)
	{
		$inventaris = $this->Barang_model->read("id = '$id'")[0];
	
		$dataStore = [
			'kode_inventaris' => $inventaris->kode_barang."".($this->Detail_barang_model->count($id) + 1),
			'satuan' => $this->input->post('satuan'),
			'status' => $this->input->post('status'),
			'nama_barang' => $this->input->post('nama_barang'),
			'id_barang' => $id
		];

		$this->Detail_barang_model->create($dataStore);
		redirect('inventaris/show/'.$id);
	}

	public function editDetailBarang($idBarang, $idDetailBarang)
	{
		$data['detail_inventaris'] = $this->Detail_barang_model->read("id = '$idDetailBarang'")[0];
		$data['view'] = 'detail_inventaris/edit';
		$this->load->view('index', $data);
	}

	public function updateDetailBarang($idBarang, $idDetailBarang)
	{
		$dataUpdate = [
			'kode_inventaris' => $this->input->post('kode_inventaris'),
			'satuan' => $this->input->post('satuan'),
			'status' => $this->input->post('status'),
			'nama_barang' => $this->input->post('nama_barang'),
		];

		$this->Detail_barang_model->update("id = '$idDetailBarang'", $dataUpdate);
		redirect('inventaris/show/'.$idBarang);

	}
}
