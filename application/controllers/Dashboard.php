<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata("username")) redirect("login");

		$this->load->model('Barang_model');
	}
	
	public function index()
	{
		$data['dataInventaris'] = $this->Barang_model->read();
        $data['view'] = 'dashboard/index';
        $this->load->view('index', $data);
	}
}
