<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata("username")) redirect("login");
	}
	
	public function index()
	{
        $data['view'] = 'grafik/index';
        $this->load->view('index', $data);
	}
}
