<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata("username")) redirect("login");
		$this->load->model('Barang_model');
		$this->load->model('Detail_barang_model');
		$this->load->model('Monitoring_model');
	}

	public function index()
	{
		$data['dataInventaris'] = $this->Barang_model->read();
        $data['view'] = 'monitoring/index';
        $this->load->view('index', $data);
	}

	public function create($idBarang, $idDetailBarang)
	{
		$data['view'] = 'monitoring/create';
		$this->load->view('index', $data);
	}
	
	public function store($idBarang, $idDetailBarang)
	{
		$this->Detail_barang_model->update("id = '$idDetailBarang'", array('status' => $this->input->post('status')));
		$dataStore = array(
			'id_detail_barang' => $idDetailBarang,
			'id_user' => $this->session->userdata("id_user"),
			'tanggal_monitoring' => $this->input->post('tanggal_monitoring'),
			'status' => $this->input->post('status'),
			'keterangan' => $this->input->post('keterangan'),
		);

		$this->Monitoring_model->create($dataStore);

		return redirect('monitoring/show/'.$idBarang);
	}
	public function show($id)
	{
		$data['view'] = 'monitoring/show';
		$data['inventaris'] = $this->Barang_model->read("id = '$id'")[0];
		$data['dataInventaris'] = $this->Detail_barang_model->read("id_barang = '$id'");
		$this->load->view('index', $data);
	}

	public function history($idBarang, $idDetailBarang)
	{
		$data['view'] = 'monitoring/histori';
		$data['dataMonitoring'] = $this->Monitoring_model->read("id_detail_barang = '$idDetailBarang'");
		$this->load->view('index', $data);
	}
}
