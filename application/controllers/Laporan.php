<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//load Spout Library
require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
//lets Use the Spout Namespaces
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata("username")) redirect("login");
	}
	
	public function index()
	{
        $data['view'] = 'laporan/index';
        $this->load->view('index', $data);
	}

	public function exportMonitoring()
	{
		$tanggal_awal = $this->input->get('tanggal_awal');
		$tanggal_akhir = $this->input->get('tanggal_awal');
		
		$data = $this->db->select("detail_barang.kode_inventaris, detail_barang.nama_barang,monitoring.tanggal_monitoring,monitoring.status,monitoring.keterangan")
					->join("detail_barang", "detail_barang.id = monitoring.id_detail_barang")
					->where("tanggal_monitoring >=", $tanggal_awal)
					->where("tanggal_monitoring <=", $tanggal_akhir)
					->get('monitoring')->result();

		$resultData = [];
		$writer = WriterFactory::create(Type::XLSX);

		//stream to browser
		$writer->openToBrowser("laporan monitoring ".date('d-m-Y',strtotime($tanggal_awal))." sd ".date('d-m-Y',strtotime($tanggal_akhir)).".xlsx");

		$header = [
			'No',
			'Kode Inventaris',
			'Nama Barang',
			'Tanggal Monitoring',
			'Status',
			'Keterangan'
		];
		
		$writer->addRow($header); // add a row at a time

		foreach($data as $value => $result) {
			if($result->status == 1) {
				$status = 'baik';
			} elseif($result->status == 2) {
				$status = 'rusak';
			} else {
				$status = 'perbaikan';
			}

			array_push($resultData,[
				$value + 1,
				$result->kode_inventaris,
				$result->nama_barang,
				date('d-m-Y',strtotime($result->tanggal_monitoring)),
				$status,
				$result->keterangan
			]);
		}

		$writer->addRows($resultData); // add multiple rows at a time

        $writer->close();
	}
}
