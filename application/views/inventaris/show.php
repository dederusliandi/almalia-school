<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Detail Inventaris</li>
    </ol>

    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('inventaris') ?>" class="btn btn-sm btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <tbody>
                        <tr>
                            <th>Kode Barang</th>
                            <td><?= $inventaris->kode_barang ?></td>
                        </tr>
                        <tr>
                            <th>Kategori Barang</th>
                            <td><?= $inventaris->kategori_barang ?></td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengadaan</th>
                            <td><?= $inventaris->tanggal_pengadaan ?></td>
                        </tr>
                        <tr>
                            <th>Jumlah Barang</th>
                            <td><?= $inventaris->jumlah_barang ?></td>
                        </tr>
                        <tr>
                            <th>Harga Barang</th>
                            <td><?= number_format($inventaris->harga_barang,2) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <?php
                if($this->session->userdata("id_level") != 2) {
                    ?>
                        <a href="<?= site_url('inventaris/show/'.$this->uri->segment(3).'/create') ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>                        
                    <?php
                }
            ?>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Inventaris</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Status</th>
                            <?php
                                if($this->session->userdata("id_level") != 2) {
                                    echo "<th>Aksi</th>";   
                                }
                            ?>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kode Inventaris</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Status</th>
                            <?php
                                if($this->session->userdata("id_level") != 2) {
                                    echo "<th>Aksi</th>";   
                                }
                            ?>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                            foreach($dataInventaris as $value => $inventaris) {
                                ?>
                                    <tr>
                                        <td><?= $value + 1 ?></td>
                                        <td><?= $inventaris->kode_inventaris ?></td>
                                        <td><?= $inventaris->nama_barang ?></td>
                                        <td><?= $inventaris->satuan ?></td>
                                        <td>
                                            <?php
                                                if($inventaris->status == 1) {
                                                    echo "baik";
                                                } elseif($inventaris->status == 2) {
                                                    echo "rusak";
                                                } else {
                                                    echo "perbaikan";
                                                }
                                            ?>
                                        </td>
                                            <?php
                                                if($this->session->userdata("id_level") != 2) {
                                                    ?>
                                                        <td>
                                                            <a href="<?= site_url('inventaris/show/'.$this->uri->segment(3).'/edit/'.$inventaris->id) ?>" class="btn btn-warning btn-sm">Edit</a>
                                                        </td>
                                                    <?php
                                                }
                                            ?>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>