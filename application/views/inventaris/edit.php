<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Edit Data</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('inventaris') ?>" class="btn btn-success btn-sm"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" method="POST" action="<?= site_url('inventaris/update/'.$inventaris->id)?>">
                <div class="box-body">
                    <div class="form-group">
                        <label>Kode Barang</label>
                        <input type="text" class="form-control" name="kode_barang" placeholder="Kode Barang" required value="<?= $inventaris->kode_barang ?>">
                    </div>
                    <div class="form-group">
                        <label>Kategori Barang</label>
                        <select name="kategori_barang" id="" class="form-control">
                            <option value="">[ Kategori Barang ]</option>
                            <option value="Perlengkapan Kelas" <?= $inventaris->kategori_barang == "Perlengkapan Kelas" ? "selected" : "" ?>>Perlengkapan Kantor</option>
                            <option value="Perlengkapan Kantor" <?= $inventaris->kategori_barang == "Perlengkapan Kantor" ? "selected" : "" ?>>Perlengkapan Kelas</option>
                            <option value="Buku Bacaan" <?= $inventaris->kategori_barang == "Buku Bacaan" ? "selected" : "" ?>>Buku Bacaan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengadaan</label>
                        <input type="date" class="form-control" name="tanggal_pengadaan" placeholder="Tanggal Pengadaan" required value="<?= $inventaris->tanggal_pengadaan ?>">
                    </div>
                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input type="number" class="form-control" name="jumlah_barang" placeholder="Jumlah Barang" required value="<?= $inventaris->jumlah_barang ?>">
                    </div>
                    <div class="form-group">
                        <label>Harga Satuan</label>
                        <input type="number" class="form-control" name="harga_barang" placeholder="Harga Satuan" required value="<?= $inventaris->harga_barang ?>">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>