<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Tambah Data</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('inventaris') ?>" class="btn btn-success btn-sm"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" method="POST" action="<?= site_url('inventaris/store')?>">
                <div class="box-body">
                    <div class="form-group">
                        <label>Kode Barang</label>
                        <input type="text" class="form-control" name="kode_barang" placeholder="Kode Barang" required>
                    </div>
                    <div class="form-group">
                        <label>Kategori Barang</label>
                        <select name="kategori_barang" id="" class="form-control">
                            <option value="">[ Kategori Barang ]</option>
                            <option value="Perlengkapan Kantor">Perlengkapan Kantor</option>
                            <option value="Perlengkapan Kantor">Perlengkapan Kelas</option>
                            <option value="Buku Bacaan">Buku Bacaan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengadaan</label>
                        <input type="date" class="form-control" name="tanggal_pengadaan" placeholder="Tanggal Pengadaan" required>
                    </div>
                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input type="number" class="form-control" name="jumlah_barang" placeholder="Jumlah Barang" required>
                    </div>
                    <div class="form-group">
                        <label>Harga Satuan</label>
                        <input type="number" class="form-control" name="harga_barang" placeholder="Harga Satuan" required>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>