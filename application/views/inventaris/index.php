<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Tables</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <?php
                if($this->session->userdata("id_level") != 2) {
                    ?>
                        <a href="<?= site_url('inventaris/create') ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>                        
                    <?php
                }
            ?>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Kategori</th>
                            <th>Tanggal Pengadaan</th>
                            <th>Jumlah Barang</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Kategori</th>
                            <th>Tanggal Pengadaan</th>
                            <th>Jumlah Barang</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                            foreach($dataInventaris as $value => $inventaris) {
                                ?>
                                    <tr>
                                        <td><?= $value + 1 ?></td>
                                        <td><?= $inventaris->kode_barang ?></td>
                                        <td><?= $inventaris->kategori_barang ?></td>
                                        <td><?= date('d-m-Y', strtotime($inventaris->tanggal_pengadaan)) ?></td>
                                        <td><?= $inventaris->jumlah_barang ?></td>
                                        <td><?= number_format($inventaris->harga_barang,2) ?></td>
                                        <td>
                                        <?php
                                            if($this->session->userdata("id_level") == 1) {
                                                ?>
                                                    <a href="<?= site_url('inventaris/edit/'.$inventaris->id) ?>" class="btn btn-warning btn-sm">Edit</a>
                                                <?php
                                            }
                                        ?>
                                        <a href="<?= site_url('inventaris/show/'.$inventaris->id) ?>" class="btn btn-info btn-sm">Detail</a>
                                        </td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>