<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Monitoring</a>
      </li>
      <li class="breadcrumb-item active">Tambah Data</li>
    </ol>

    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('monitoring/show/'.$this->uri->segment(3)) ?>" class="btn btn-success btn-sm"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" method="POST" action="<?= site_url('monitoring/show/'.$this->uri->segment(3).'/store/'.$this->uri->segment(5))?>">
                <div class="box-body">
                    <div class="form-group">
                        <label>Tanggal Monitoring</label>
                        <input type="date" class="form-control" name="tanggal_monitoring" placeholder="Tanggal Monitoring" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" id="" class="form-control" required>
                            <option value="">[ Pilih Status ]</option>
                            <option value="1">Baik</option>
                            <option value="2">Rusak</option>
                            <option value="3">Perbaikan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea class="form-control" name="keterangan" placeholder="Keterangan" required></textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>