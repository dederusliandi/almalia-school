<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Monitoring</a>
      </li>
      <li class="breadcrumb-item active">Tables</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('monitoring/show/'.$this->uri->segment(3)) ?>" class="btn btn-success btn-sm"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Monitoring</th>
                            <th>Status</th>
                            <th>Petugas Monitoring</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Monitoring</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                            foreach($dataMonitoring as $value => $monitoring) {
                                ?>
                                    <tr>
                                        <td><?= $value + 1 ?></td>
                                        <td><?= date('d-m-Y', strtotime($monitoring->tanggal_monitoring)) ?></td>
                                        <td>
                                            <?php
                                                if($monitoring->status == 1) {
                                                    echo "baik";
                                                } elseif($monitoring->status == 2) {
                                                    echo "rusak";
                                                } else {
                                                    echo "perbaikan";
                                                }
                                            ?>
                                        </td>
                                        <td><?= $monitoring->nama_lengkap ?></td>
                                        <td><?= $monitoring->keterangan ?></td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>