<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Detail Inventaris</li>
    </ol>

    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('monitoring') ?>" class="btn btn-sm btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <tbody>
                        <tr>
                            <th>Kode Barang</th>
                            <td><?= $inventaris->kode_barang ?></td>
                        </tr>
                        <tr>
                            <th>Kategori Barang</th>
                            <td><?= $inventaris->kategori_barang ?></td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengadaan</th>
                            <td><?= $inventaris->tanggal_pengadaan ?></td>
                        </tr>
                        <tr>
                            <th>Jumlah Barang</th>
                            <td><?= $inventaris->jumlah_barang ?></td>
                        </tr>
                        <tr>
                            <th>Harga Barang</th>
                            <td><?= number_format($inventaris->harga_barang,2) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            Data Inventaris
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Inventaris</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kode Inventaris</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                            foreach($dataInventaris as $value => $inventaris) {
                                ?>
                                    <tr>
                                        <td><?= $value + 1 ?></td>
                                        <td><?= $inventaris->kode_inventaris ?></td>
                                        <td><?= $inventaris->nama_barang ?></td>
                                        <td><?= $inventaris->satuan ?></td>
                                        <td>
                                            <?php
                                                if($inventaris->status == 1) {
                                                    echo "baik";
                                                } elseif($inventaris->status == 2) {
                                                    echo "rusak";
                                                } else {
                                                    echo "perbaikan";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if($this->session->userdata("id_level") != 3) {
                                                    ?>
                                                        <a href="<?= site_url('monitoring/show/'.$this->uri->segment(3).'/create/'.$inventaris->id) ?>" class="btn btn-success btn-sm">Monitoring Data</a>
                                                    <?php
                                                }
                                            ?>
                                            <a href="<?= site_url('monitoring/show/'.$this->uri->segment(3).'/history/'.$inventaris->id) ?>" class="btn btn-info btn-sm">Histori Monitoring</a>
                                        </td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>