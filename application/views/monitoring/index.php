<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Monitoring</a>
      </li>
      <li class="breadcrumb-item active">Tables</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            Data Monitoring
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Kategori</th>
                            <th>Tanggal Pengadaan</th>
                            <th>Jumlah Barang</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Kategori</th>
                            <th>Tanggal Pengadaan</th>
                            <th>Jumlah Barang</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                            foreach($dataInventaris as $value => $inventaris) {
                                ?>
                                    <tr>
                                        <td><?= $value + 1 ?></td>
                                        <td><?= $inventaris->kode_barang ?></td>
                                        <td><?= $inventaris->kategori_barang ?></td>
                                        <td><?= date('d-m-Y', strtotime($inventaris->tanggal_pengadaan)) ?></td>
                                        <td><?= $inventaris->jumlah_barang ?></td>
                                        <td><?= number_format($inventaris->harga_barang,2) ?></td>
                                        <td>
                                            <a href="<?= site_url('monitoring/show/'.$inventaris->id) ?>" class="btn btn-info btn-sm">Detail</a>
                                        </td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>