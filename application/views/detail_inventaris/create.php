<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Tambah Data</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <a href="<?= site_url('inventaris/show/'.$this->uri->segment(3)) ?>" class="btn btn-success btn-sm"><i class="fa fa-chevron-left"></i> Kembali</a>
        </div>
        <div class="card-body">
            <form role="form" method="POST" action="<?= site_url('inventaris/show/'.$this->uri->segment(3).'/store')?>">
                <div class="box-body">
                    <div class="form-group">
                        <label>Kode Inventaris</label>
                        <input type="text" class="form-control" name="kode_inventaris" placeholder="Kode Inventaris" required value="<?= $kode_inventaris ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang" required>
                    </div>
                    <div class="form-group">
                        <label>Satuan</label>
                        <input type="text" class="form-control" name="satuan" placeholder="Satuan" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" id="" class="form-control">
                            <option value="1">baik</option>
                            <option value="2">rusak</option>
                            <option value="3">perbaikan</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>