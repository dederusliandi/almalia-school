<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Inventaris</a>
      </li>
      <li class="breadcrumb-item active">Tables</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            Grafik Inventaris
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div id="chartdiv-count" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
</div>