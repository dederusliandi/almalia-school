<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Almalia School</title>
    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="<?= base_url('assets/vendor/datatables/dataTables.bootstrap4.css') ?>" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/css/sb-admin.css') ?>" rel="stylesheet">
</head>

<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="index.html">Welcome to Almalia School : Daycare and Preschool</a>
        <!-- <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button> -->

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
          <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                      <i class="fas fa-search"></i>
                  </button>
              </div>
          </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
          <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-bell fa-fw"></i>
                  <span class="badge badge-danger">9+</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
              </div>
          </li>
          <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-envelope fa-fw"></i>
                  <span class="badge badge-danger">7</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
              </div>
          </li>
          <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user-circle fa-fw"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?= site_url('login/logout') ?>" data-toggle="modal" data-target="#logoutModal">Logout</a>
              </div>
          </li>
      </ul>

    </nav>

    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item <?= $this->uri->segment(1) == "dashboard" ? "active" : ""?>">
                <a class="nav-link" href="<?= site_url('dashboard') ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item <?= $this->uri->segment(1) == "inventaris" ? "active" : ""?>">
                <a class="nav-link" href="<?= site_url('inventaris') ?>">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Data Inventaris Barang</span>
                </a>
            </li>
            <li class="nav-item <?= $this->uri->segment(1) == "laporan" ? "active" : ""?>">
                <a class="nav-link" href="<?= site_url('laporan') ?>">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Laporan Inventaris Barang</span>
                </a>
            </li>
            <li class="nav-item <?= $this->uri->segment(1) == "grafik" ? "active" : ""?>">
                <a class="nav-link" href="<?= site_url('grafik') ?>">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Grafik Inventaris Barang</span>
                </a>
            </li>
            <li class="nav-item <?= $this->uri->segment(1) == "monitoring" ? "active" : "" ?>">
                <a class="nav-link" href="<?= site_url('monitoring') ?>">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Data Monitoring Inventaris Barang</span>
                </a>
            </li>
        </ul>

        <div id="content-wrapper">
            <?php
              $this->load->view($view);
            ?>
            <!-- /.container-fluid -->

            <!-- Sticky Footer -->
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © Your Website 2019</span>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="<?= site_url('login/logout') ?>">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

    <!-- Page level plugin JavaScript-->
    <script src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js') ?>"></script>
    <script src="<?= base_url('assets/vendor/datatables/dataTables.bootstrap4.js') ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/js/sb-admin.min.js') ?>"></script>

    <!-- Demo scripts for this page-->
    <script src="<?= base_url('assets/js/demo/datatables-demo.js') ?>"></script>

    <script src="<?= base_url('assets/amcharts/amcharts.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/amcharts/serial.js') ?>" type="text/javascript"></script>

        <script>
                var chart;
                var chartData = [
                    {
                        "date": ["01/10/2019","01/10/2019"],
                        "count" : 13,
                    },
                    {
                        "date": "02/10/2019",
                        "count" : 56,
                    },
                    {
                        "date": "03/10/2019",
                        "count" : 40,
                    },
                    {
                        "date": "04/10/2019",
                        "count" : 10,
                    },
                    {
                        "date": "05/10/2019",
                        "count" : 21,
                    },
                    {
                        "date": "06/10/2019",
                        "count" : 54,
                    },
                    {
                        "date": "07/10/2019",
                        "count" : 78,
                    },
                    {
                        "date": "08/10/2019",
                        "count" : 5,
                    },
                    {
                        "date": "09/10/2019",
                        "count" : 20,
                    },
                    {
                        "date": "10/10/2019",
                        "count" : 30,
                    },
                ];


                AmCharts.ready(function () {
                    // SERIAL CHART
                    chart = new AmCharts.AmSerialChart();
                    chart.dataProvider = chartData;
                    chart.categoryField = "date";
                    chart.startDuration = 1;

                    // AXES
                    // category
                    var categoryAxis = chart.categoryAxis;
                    categoryAxis.labelRotation = 30;
                    categoryAxis.gridPosition = "start";

                    // value
                    // in case you don't want to change default settings of value axis,
                    // you don't need to create it, as one value axis is created automatically.

                    // GRAPH
                    var graph = new AmCharts.AmGraph();
                    graph.valueField = "count";
                    graph.balloonText = "[[category]]: <b>[[value]]</b>";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 0.8;
                    chart.addGraph(graph);

                    // CURSOR
                    var chartCursor = new AmCharts.ChartCursor();
                    chartCursor.cursorAlpha = 0;
                    chartCursor.zoomable = false;
                    chartCursor.categoryBalloonEnabled = false;
                    graph.fillColors = '#00a65a';
                    chart.addChartCursor(chartCursor);

                    // SCROLLBAR
                    var chartScrollbar = new AmCharts.ChartScrollbar();
                    chart.addChartScrollbar(chartScrollbar);

                    chart.creditsPosition = "top-right";

                    chart.write("chartdiv-count");
                });
            </script>

</body>

</html>
